package com.company;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import java.util.Arrays;
import static org.junit.Assert.*;

public class MainTest {

    @RunWith(Parameterized.class)
    public static class IntToStringTest {
        private int valueA;
        private String expected;

        public IntToStringTest(int valueA, String expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {1, "1"},
                    {2, "2"},
                    {77, "77"},
                    {11, "11"},
                    {0, "0"}
            });
        }

        @Test
        public void intToStringTest_Pass() {
            assertEquals(expected, new Main().intToString(valueA));
        }
    }

    @RunWith(Parameterized.class)
    public static class doubleToStringTest {
        private double valueA;
        private String expected;

        public doubleToStringTest(double valueA, String expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {1.0, "1.0"},
                    {2.0, "2.0"},
                    {66.0, "66.0"},
                    {20.0, "20.0"},
                    {11.0, "11.0"}
            });
        }

        @Test
        public void doubleToString_Pass() {
            assertEquals(expected, new Main().doubleToString(valueA));
        }
    }
    @RunWith(Parameterized.class)
    public static class stringToInt {
        private String valueA;
        private int expected;

        public stringToInt(String valueA, int expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"1", 1},
                    {"2", 2},
                    {"55", 55},
                    {"20", 20},
                    {"0", 0}
            });
        }
        @Test
        public void stringToInt_Pass() {
            assertEquals(expected, new Main().stringToInt(valueA));
        }
    }
    @RunWith(Parameterized.class)
    public static class stringToDouble{
        private String valueA;
        private double expected;
        private double delta = 0.0;

        public stringToDouble(String valueA, double expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"1.0", 1.0},
                    {"2.2", 2.2},
                    {"56.5", 56.5},
                    {"20.7", 20.7},
                    {"11.0", 11.0}
            });
        }
        @Test
        public void stringToDouble_Pass() {
            assertEquals(expected, new Main().stringToDouble(valueA),delta);
        }
    }
    @RunWith(Parameterized.class)
    public static class deleteLastWord {
        private String valueA;
        private String expected;

        public deleteLastWord(String valueA, String expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"delet last word", "delet last"},
                    {"one two three", "one two"},
                    {"55 4 55", "55 4"}
            });
        }
        @Test
        public void deletLastWord_Pass() {
            assertEquals(expected, new Main().deleteLastWord(valueA));
        }
    }
    @RunWith(Parameterized.class)
    public static class howManyWords {
        private String valueA;
        private int expected;

        public howManyWords(String valueA, int expected) {
            this.valueA = valueA;
            this.expected = expected;
        }

        @Parameterized.Parameters(name = "")
        public static Iterable<Object[]> dataForTest() {
            return Arrays.asList(new Object[][]{
                    {"1 4 5 2 5", 5},
                    {"some text", 2},
                    {"bla bla bla", 3}
            });
        }
        @Test
        public void howManyWords_Pass() {
            assertEquals(expected, new Main().howManyWords(valueA));
        }
    }
}