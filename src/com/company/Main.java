package com.company;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
	printABC();
	printZYX();
	printRU();
	printNumbers();
	intToString(5);
	doubleToString(4.6);
	stringToInt("3");
	stringToDouble("9.4");
	lengthOfShortWord("dsfe ffff fsfsf ");
	addProbel();
	howManyWords();
	deletSomePartOfString();
	reverseString();
	deleteLastWord();
    }
    public static void printABC(){
        System.out.println("A B C D E F G H I J K L M N O P Q R S T U V W X Y Z");
    }
    public static void printZYX(){
        System.out.println("z y x w v u t s r q p o n m l k j i h g f e d c b a");
    }
    public static void printRU(){
        System.out.println("а б в г д е ё ж з и й к л м н о п р с т у ф х ц ч ш щ ы ь э ю я");
    }
    public static void printNumbers(){
        System.out.println("0 1 2 3 4 5 6 7 8 9 0");
    }
    public static void intToString(int number){
        String str = Integer.toString(number);
        System.out.println(str);
    }
    public static void doubleToString(double number){
        String str = Double.toString(number);
        System.out.println(str);
    }
    public static void stringToInt(String str){
        int number = Integer.parseInt(str);
        System.out.println(number);
    }
    public static void stringToDouble(String str){
        double number = Double.parseDouble(str);
        System.out.println(number);
    }

    public static void lengthOfShortWord(String str){
        String [] slova = str.split(" ");
        int [] Mass = new int[slova.length];
        for(int i = 0; i<slova.length; i++) {
            if (slova[i] != " ") {
                Mass[i] = slova[i].length();

                int min = Mass[0];
                if (min < Mass[i])
                    System.out.println("Minimal element: " + min);
            }
        }
        }
        public static void addProbel(){
            Scanner in = new Scanner(System.in);
            System.out.println("Введите строку: ");
            char[] charIn = {'.',','};
            StringBuilder sb=new StringBuilder();//создаем объект стрингбилдер
            sb.append(in.nextLine()); //вносим в стрингбилдер то, что пользователь введет с клавиатуры
            for (int i=1; i<sb.length()-1;i++) { //посимвольно просматриваем введенную строку
                for (char ch:charIn) { // сравниваем с массивом знаков препинания
                    if (sb.charAt(i)==ch) { // если символы совпали и ...
                        if (!sb.substring(i+1, i+2).equals(" ")) { // ... следующий после знака препинания символ НЕ пробел...
                            sb.insert(i+1, ' ');// вставляем пробел
                        }

                    }
                }
            }
            System.out.println(sb.toString()); //выводим результат

        }
    public static  void howManyWords(){
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите слова одной строкой через пробел");
        String input = sc.nextLine();
        int count = 0;
        if(input.length() != 0){
            count++;
            for (int i = 0; i < input.length(); i++) {
                if(input.charAt(i) == ' '){
                    count++;
                }
            }
        }

        System.out.println("Вы ввели "+count+"слов");
    }
    public static void deletSomePartOfString(){
        Scanner in = new Scanner (System.in);
        String source, delete;
        System.out.println("Введите строку ");
        source = in.nextLine();
        System.out.println("Введите что нужно удалить ");
        delete = in.nextLine();
        source = source.replace (delete, "");
        System.out.println (source);
    }
    public static void reverseString(){
        StringBuffer buffer = new StringBuffer("Весёлое Программирование");
        buffer.reverse();
        System.out.println(buffer);
    }
    public static void deleteLastWord(){
        var str = "I want to remove the last word.";
        var lastIndex = str.lastIndexOf(" ");
        str = str.substring(0, lastIndex);
        System.out.println(str);
    }
}

